# hselect2
Hierarchical Select for Drupal 8 using Select2.

This module is based on shs and hierarchical_select.

The widget for taxonomy term reference works using the deepest selection.

On D8 you can select multiple Vocabularies. Please select only one for the moment :)

Select2 library will be removed from this module. Probably Select2 will be added to D8 core and for the moment I'm using this library here.